package pl.dram.applauseproject.datamanager;

import org.junit.jupiter.api.Test;
import pl.dram.applauseproject.datamanager.csvreader.CsvFileReaderUtils;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.DeviceMapper;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class CsvFileReaderUtilsTest {
    @Test
    void shouldThrowExceptionWhileFileNotFound(){
        CsvFileReaderUtils csvFileReaderUtils = new CsvFileReaderUtils();

        assertThrows(IllegalArgumentException.class, () -> csvFileReaderUtils.readFileAndMap("dummyFileNameasdas", new DeviceMapper()));
    }
}
