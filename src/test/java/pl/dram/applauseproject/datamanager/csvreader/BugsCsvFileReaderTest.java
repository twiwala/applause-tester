package pl.dram.applauseproject.datamanager.csvreader;

import org.junit.jupiter.api.Test;
import pl.dram.applauseproject.config.Config;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.BugRow;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.BugsMapper;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BugsCsvFileReaderTest {
    private CsvFileReaderUtils csvFileReaderUtils = new CsvFileReaderUtils();

    @Test
    void shouldReadAllRowsFromBugsExceptHeader() {
        List<BugRow> testerRows = csvFileReaderUtils.readFileAndMap(Config.BUGS_FILE_NAME, new BugsMapper());

        assertEquals(1000, testerRows.size());
    }
    @Test
    void shouldMapRowsInAProperWay() {
        List<BugRow> testerRows = csvFileReaderUtils.readFileAndMap(Config.BUGS_FILE_NAME, new BugsMapper());

        BugRow bugRow = testerRows.get(2);
        assertEquals(3, bugRow.getBugId());
        assertEquals(4, bugRow.getDeviceId());
        assertEquals(7, bugRow.getTesterId());
    }
}
