package pl.dram.applauseproject.datamanager.csvreader;

import org.junit.jupiter.api.Test;
import pl.dram.applauseproject.config.Config;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.TesterDeviceRow;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.TesterDeviceMapper;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TesterDevicesCsvFileReaderTest {
    private CsvFileReaderUtils csvFileReaderUtils = new CsvFileReaderUtils();

    @Test
    void shouldReadAllRowsFromTesterDevicesExceptHeader() {
        List<TesterDeviceRow> testerRows = csvFileReaderUtils.readFileAndMap(Config.TESTERS_DEVICES_FILE_NAME, new TesterDeviceMapper());

        assertEquals(36, testerRows.size());
    }

    @Test
    void shouldMapRowsInAProperWay() {
        List<TesterDeviceRow> testerRows = csvFileReaderUtils.readFileAndMap(Config.TESTERS_DEVICES_FILE_NAME, new TesterDeviceMapper());

        TesterDeviceRow testerDeviceRow = testerRows.get(2);
        assertEquals(3, testerDeviceRow.getDeviceId());
        assertEquals(1, testerDeviceRow.getTesterId());
    }
}
