package pl.dram.applauseproject.datamanager.csvreader;

import org.junit.jupiter.api.Test;
import pl.dram.applauseproject.config.Config;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.TesterRow;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.TesterMapper;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TesterCsvFileReaderTest {
    private CsvFileReaderUtils csvFileReaderUtils = new CsvFileReaderUtils();

    @Test
    void shouldReadAllRowsFromTestersExceptHeader() {
        List<TesterRow> testerRows = csvFileReaderUtils.readFileAndMap(Config.TESTERS_FILE_NAME, new TesterMapper());

        assertEquals(9, testerRows.size());
    }

    @Test
    void shouldMapRowsInAProperWay() {
        List<TesterRow> testerRows = csvFileReaderUtils.readFileAndMap(Config.TESTERS_FILE_NAME, new TesterMapper());

        TesterRow testerDeviceRow = testerRows.get(2);
        assertEquals(3, testerDeviceRow.getTesterId());
        assertEquals("Leonard", testerDeviceRow.getFirstName());
        assertEquals("Sutton", testerDeviceRow.getLastName());
        assertEquals("GB", testerDeviceRow.getCountry());
    }
}
