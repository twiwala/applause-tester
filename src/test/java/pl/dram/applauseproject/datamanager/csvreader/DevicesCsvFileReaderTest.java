package pl.dram.applauseproject.datamanager.csvreader;

import org.junit.jupiter.api.Test;
import pl.dram.applauseproject.config.Config;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.DeviceRow;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.DeviceMapper;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DevicesCsvFileReaderTest {
    private CsvFileReaderUtils csvFileReaderUtils = new CsvFileReaderUtils();

    @Test
    void shouldReadAllRowsFromDevicesExceptHeader() {
        List<DeviceRow> testerRows = csvFileReaderUtils.readFileAndMap(Config.DEVICES_FILE_NAME, new DeviceMapper());

        assertEquals(10, testerRows.size());
    }

    @Test
    void shouldMapRowsInAProperWay() {
        List<DeviceRow> testerRows = csvFileReaderUtils.readFileAndMap(Config.DEVICES_FILE_NAME, new DeviceMapper());

        DeviceRow deviceRow = testerRows.get(2);
        assertEquals(3, deviceRow.getDeviceId());
        assertEquals("iPhone 5", deviceRow.getDescription());
    }
}
