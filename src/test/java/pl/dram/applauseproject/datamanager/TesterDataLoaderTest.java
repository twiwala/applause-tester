package pl.dram.applauseproject.datamanager;

import org.junit.jupiter.api.Test;
import pl.dram.applauseproject.datamanager.csvreader.CsvFileReaderUtils;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class TesterDataLoaderTest {
    @Test
    void shouldCreateTesterLoaderWithLoadedData(){
        TesterDataLoader testerDataLoader = new TesterDataLoader(new DataLoader(new CsvFileReaderUtils()));

        assertNotNull(testerDataLoader);
    }
}
