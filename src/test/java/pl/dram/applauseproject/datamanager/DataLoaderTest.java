package pl.dram.applauseproject.datamanager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.dram.applauseproject.config.Config;
import pl.dram.applauseproject.datamanager.csvreader.CsvFileReaderUtils;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.BugRow;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.DeviceRow;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.TesterDeviceRow;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.TesterRow;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.BugsMapper;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.DeviceMapper;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.TesterDeviceMapper;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.TesterMapper;
import pl.dram.applauseproject.model.Tester;
import pl.dram.applauseproject.selector.SelectResult;
import pl.dram.applauseproject.selector.filters.TesterFilter;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DataLoaderTest {
    private static final DeviceRow DEVICE_NEXUS_4 = new DeviceRow(6, "Nexus 4");
    private static final DeviceRow DEVICE_DROID_RAZOR = new DeviceRow(7, "Droid Razor");

    private static final TesterRow TESTER_US = new TesterRow(1, "Will", "Smith", "US");
    private static final TesterRow TESTER_PL = new TesterRow(2, "Jan", "Kowalski", "PL");

    @Mock
    private CsvFileReaderUtils csvFileReaderUtils;
    @BeforeEach
    void setUp() {
        when(csvFileReaderUtils.readFileAndMap(eq(Config.BUGS_FILE_NAME), any(BugsMapper.class))).then(e ->
                List.of(
                        new BugRow(1, DEVICE_NEXUS_4.getDeviceId(), TESTER_US.getTesterId()),
                        new BugRow(2, DEVICE_NEXUS_4.getDeviceId(), TESTER_PL.getTesterId()),
                        new BugRow(3, DEVICE_DROID_RAZOR.getDeviceId(), TESTER_PL.getTesterId()),
                        new BugRow(4, DEVICE_NEXUS_4.getDeviceId(), TESTER_US.getTesterId()),
                        new BugRow(5, DEVICE_DROID_RAZOR.getDeviceId(), TESTER_PL.getTesterId()),
                        new BugRow(6, DEVICE_NEXUS_4.getDeviceId(), TESTER_US.getTesterId()),
                        new BugRow(7, DEVICE_DROID_RAZOR.getDeviceId(), TESTER_PL.getTesterId())
                )
        );
        when(csvFileReaderUtils.readFileAndMap(eq(Config.TESTERS_FILE_NAME), any(TesterMapper.class))).then(e ->
                List.of(TESTER_US, TESTER_PL)
        );
        when(csvFileReaderUtils.readFileAndMap(eq(Config.TESTERS_DEVICES_FILE_NAME), any(TesterDeviceMapper.class))).then(e ->
                List.of(
                        new TesterDeviceRow(TESTER_PL.getTesterId(), DEVICE_NEXUS_4.getDeviceId()),
                        new TesterDeviceRow(TESTER_PL.getTesterId(), DEVICE_DROID_RAZOR.getDeviceId()),
                        new TesterDeviceRow(TESTER_US.getTesterId(), DEVICE_NEXUS_4.getDeviceId())
                )
        );
        when(csvFileReaderUtils.readFileAndMap(eq(Config.DEVICES_FILE_NAME), any(DeviceMapper.class))).then(e ->
                List.of(DEVICE_NEXUS_4, DEVICE_DROID_RAZOR)
        );
    }

    @Test
    void shouldReadTestersAndJoinThemWithDevices(){
        DataLoader dataLoader = new DataLoader(csvFileReaderUtils);
        dataLoader.loadData();

        List<Tester> testers = dataLoader.getTesters();
        assertEquals(2, testers.size());
    }

    @Test
    void shouldCalculateExperienceForTestersWhileNoFilter(){
        TesterFilter testerFilter = new TesterFilter.TesterFilterBuilder()
                .countryFilter("ALL")
                .deviceFilter("ALL")
                .build();

        List<SelectResult> resultByFilter = new TesterDataLoader(new DataLoader(csvFileReaderUtils)).getResultByFilter(testerFilter);
        assertEquals(2, resultByFilter.size());
        assertEquals(4, resultByFilter.get(0).getExperience());
        assertEquals(3, resultByFilter.get(1).getExperience());
    }
    @Test
    void shouldCalculateExperienceForCountryFilter(){
        TesterFilter testerFilter = new TesterFilter.TesterFilterBuilder()
                .countryFilter("PL")
                .deviceFilter("ALL")
                .build();

        List<SelectResult> resultByFilter = new TesterDataLoader(new DataLoader(csvFileReaderUtils)).getResultByFilter(testerFilter);
        assertEquals(1, resultByFilter.size());
        assertEquals(4, resultByFilter.get(0).getExperience());
    }
    @Test
    void shouldCalculateExperienceForMultipleCountryFilter(){
        TesterFilter testerFilter = new TesterFilter.TesterFilterBuilder()
                .countryFilter("PL,US")
                .deviceFilter("ALL")
                .build();

        List<SelectResult> resultByFilter = new TesterDataLoader(new DataLoader(csvFileReaderUtils)).getResultByFilter(testerFilter);
        assertEquals(2, resultByFilter.size());
        assertEquals(4, resultByFilter.get(0).getExperience());
        assertEquals(3, resultByFilter.get(1).getExperience());
    }
    @Test
    void shouldCalculateExperienceForCountryAndDeviceFilter(){
        TesterFilter testerFilter = new TesterFilter.TesterFilterBuilder()
                .countryFilter("PL")
                .deviceFilter("Nexus 4")
                .build();

        List<SelectResult> resultByFilter = new TesterDataLoader(new DataLoader(csvFileReaderUtils)).getResultByFilter(testerFilter);
        assertEquals(1, resultByFilter.size());
        assertEquals(1, resultByFilter.get(0).getExperience());
    }
    @Test
    void shouldCalculateExperienceForCountryAndMultipleDeviceFilter(){
        TesterFilter testerFilter = new TesterFilter.TesterFilterBuilder()
                .countryFilter("PL")
                .deviceFilter("Nexus 4,Droid Razor")
                .build();

        List<SelectResult> resultByFilter = new TesterDataLoader(new DataLoader(csvFileReaderUtils)).getResultByFilter(testerFilter);
        assertEquals(1, resultByFilter.size());
        assertEquals(4, resultByFilter.get(0).getExperience());
    }

}
