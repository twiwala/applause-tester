package pl.dram.applauseproject.selector;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SelectResult {
    private String testerFirstName;
    private String testerLastName;
    private int experience;

    @Override
    public String toString() {
        return testerFirstName + " " + testerLastName + " -> " + experience;
    }
}
