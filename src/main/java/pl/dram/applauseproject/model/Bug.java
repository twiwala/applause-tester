package pl.dram.applauseproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Bug {
    private long id;
}
