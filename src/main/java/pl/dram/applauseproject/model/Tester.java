package pl.dram.applauseproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class Tester {
    private long id;
    private String firstName;
    private String lastName;
    private String country;

    private Map<Device, List<Bug>> testedDevices;
    //    private LocalDateTime lastLogin; no needed
}
