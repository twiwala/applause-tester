package pl.dram.applauseproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Device {
    private long id;
    private String desc;
}
