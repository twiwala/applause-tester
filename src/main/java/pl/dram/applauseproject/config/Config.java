package pl.dram.applauseproject.config;

public class Config {
    public static final String BUGS_FILE_NAME = "bugs.csv";
    public static final String DEVICES_FILE_NAME = "devices.csv";
    public static final String TESTERS_DEVICES_FILE_NAME = "tester_device.csv";
    public static final String TESTERS_FILE_NAME = "testers.csv";
}
