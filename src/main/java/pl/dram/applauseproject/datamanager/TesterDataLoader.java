package pl.dram.applauseproject.datamanager;

import lombok.Data;
import pl.dram.applauseproject.model.Bug;
import pl.dram.applauseproject.model.Device;
import pl.dram.applauseproject.selector.SelectResult;
import pl.dram.applauseproject.selector.filters.StringFilterOption;
import pl.dram.applauseproject.selector.filters.TesterFilter;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class TesterDataLoader {
    private DataLoader dataLoader;

    public TesterDataLoader(DataLoader dataLoader){
        this.dataLoader = dataLoader;
        dataLoader.loadData();
    }
    public List<SelectResult> getResultByFilter(TesterFilter testerFilter) {
        return dataLoader.getTesters()
                .stream()
                .filter(e -> isCountryMatchingCriteria(e.getCountry(), testerFilter.getCountries()))
                .map(e -> new SelectResult(e.getFirstName(), e.getLastName(), countExperience(e.getTestedDevices(), testerFilter)))
                .filter(e -> e.getExperience() != 0)
                .sorted(Comparator.comparing(SelectResult::getExperience).reversed())
                .collect(Collectors.toList());
    }

    private int countExperience(Map<Device, List<Bug>> testedDevices, TesterFilter testerFilter) {
        return testedDevices
                .entrySet()
                .stream()
                .filter(e -> isDeviceMatchingCriteria(e.getKey(), testerFilter.getDevices()))
                .mapToInt(e -> e.getValue().size())
                .sum();
    }
    private boolean isCountryMatchingCriteria(String country, StringFilterOption countryFilter){
        if(countryFilter.isAll()){
            return true;
        }
        return countryFilter.getValues().contains(country);
    }
    private boolean isDeviceMatchingCriteria(Device d, StringFilterOption deviceFilter) {
        if (deviceFilter.isAll()) {
            return true;
        }
        return deviceFilter.getValues().contains(d.getDesc());
    }


}
