package pl.dram.applauseproject.datamanager;

import pl.dram.applauseproject.config.Config;
import pl.dram.applauseproject.datamanager.csvreader.CsvFileReaderUtils;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.BugRow;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.DeviceRow;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.TesterDeviceRow;
import pl.dram.applauseproject.datamanager.csvreader.datamodel.TesterRow;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.BugsMapper;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.DeviceMapper;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.TesterDeviceMapper;
import pl.dram.applauseproject.datamanager.csvreader.mappers.impl.TesterMapper;
import pl.dram.applauseproject.model.Bug;
import pl.dram.applauseproject.model.Device;
import pl.dram.applauseproject.model.Tester;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DataLoader {
    private List<Tester> testers;
    private CsvFileReaderUtils csvFileReaderUtils;

    public DataLoader(CsvFileReaderUtils csvFileReaderUtils){
        this.csvFileReaderUtils = csvFileReaderUtils;
    }
    public void loadData() {
        Map<Long, Map<Long, Device>> testerIdAndDevices = readDevicesAndGroupByTesterId();
        Map<Long, Map<Long, List<Bug>>> bugsGroupByTestersAndDevice = readBugsAndGroupByTestersDevice();

        testers = readTesters(testerIdAndDevices, bugsGroupByTestersAndDevice);
    }
    public List<Tester> getTesters(){
        return testers;
    }

    private List<Tester> readTesters(Map<Long, Map<Long, Device>> testerIdAndDevices, Map<Long, Map<Long, List<Bug>>> bugsGroupByTestersAndDevice) {
        List<TesterRow> testerRows = csvFileReaderUtils.readFileAndMap(Config.TESTERS_FILE_NAME, new TesterMapper());

        return testerRows
                .stream()
                .map(e -> new Tester(
                                e.getTesterId(),
                                e.getFirstName(),
                                e.getLastName(),
                                e.getCountry(),
                                getBugsForTester(e.getTesterId(), testerIdAndDevices, bugsGroupByTestersAndDevice)
                        )
                )
                .collect(Collectors.toUnmodifiableList());
    }

    private Map<Device, List<Bug>> getBugsForTester(long testerId, Map<Long, Map<Long, Device>> testerIdAndDevices, Map<Long, Map<Long, List<Bug>>> bugsGroupByTestersAndDevice) {
        return testerIdAndDevices
                .get(testerId)
                .entrySet()
                .stream()
                .collect(
                        Collectors.toMap(Map.Entry::getValue, e -> bugsGroupByTestersAndDevice.get(testerId).get(e.getValue().getId()))
                );
    }

    //testers => device => bug
    private Map<Long, Map<Long, List<Bug>>> readBugsAndGroupByTestersDevice() {
        List<BugRow> bugRows = csvFileReaderUtils.readFileAndMap(Config.BUGS_FILE_NAME, new BugsMapper());

        return bugRows.stream()
                .collect(
                        Collectors.groupingBy(
                                BugRow::getTesterId,
                                Collectors.groupingBy(
                                        BugRow::getDeviceId,
                                        Collectors.mapping(e -> new Bug(e.getBugId()), Collectors.toList())
                                )
                        )
                );
    }

    //testerId => deviceId => device
    private Map<Long, Map<Long, Device>> readDevicesAndGroupByTesterId() {
        List<DeviceRow> deviceRows = csvFileReaderUtils.readFileAndMap(Config.DEVICES_FILE_NAME, new DeviceMapper());
        List<TesterDeviceRow> testerDeviceRows = csvFileReaderUtils.readFileAndMap(Config.TESTERS_DEVICES_FILE_NAME, new TesterDeviceMapper());

        Map<Long, Device> mapDeviceId = deviceRows.stream()
                .map(e -> new Device(e.getDeviceId(), e.getDescription()))
                .collect(Collectors.toMap(Device::getId, Function.identity()));

        return testerDeviceRows.stream()
                .collect(Collectors.groupingBy(
                        TesterDeviceRow::getTesterId,
                        Collectors.mapping(e -> mapDeviceId.get(e.getDeviceId()), Collectors.toMap(Device::getId, Function.identity()))
                ));
    }
}
