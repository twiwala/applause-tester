package pl.dram.applauseproject.datamanager.csvreader.datamodel;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DeviceRow {
    private long deviceId;
    private String description;
}
