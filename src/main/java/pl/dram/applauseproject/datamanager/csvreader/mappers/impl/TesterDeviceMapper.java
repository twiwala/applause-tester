package pl.dram.applauseproject.datamanager.csvreader.mappers.impl;

import pl.dram.applauseproject.datamanager.csvreader.datamodel.TesterDeviceRow;
import pl.dram.applauseproject.datamanager.csvreader.mappers.CsvRowMapper;

public class TesterDeviceMapper implements CsvRowMapper<TesterDeviceRow> {
    @Override
    public TesterDeviceRow rowMapper(String[] row) {
        return new TesterDeviceRow(
                Long.parseLong(row[0]),
                Long.parseLong(row[1])
        );
    }
}
