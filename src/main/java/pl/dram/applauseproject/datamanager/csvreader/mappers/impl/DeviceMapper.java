package pl.dram.applauseproject.datamanager.csvreader.mappers.impl;

import pl.dram.applauseproject.datamanager.csvreader.datamodel.DeviceRow;
import pl.dram.applauseproject.datamanager.csvreader.mappers.CsvRowMapper;

public class DeviceMapper implements CsvRowMapper<DeviceRow> {
    @Override
    public DeviceRow rowMapper(String[] row) {
        return new DeviceRow(
                Long.parseLong(row[0]),
                row[1]
        );
    }
}
