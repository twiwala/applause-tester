package pl.dram.applauseproject.datamanager.csvreader;


import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CsvFileReader implements AutoCloseable {
    private final CSVReader csvReader;

    public CsvFileReader(String fileName) throws IOException, URISyntaxException {
        URL resource = getClass().getClassLoader().getResource(fileName);
        if(resource == null){
            throw new IllegalArgumentException("File not found!");
        }
        URI uri = resource.toURI();
        Path path = Paths.get(uri);
        this.csvReader = new CSVReaderBuilder(Files.newBufferedReader(path, StandardCharsets.UTF_8))
                .withSkipLines(1) //omit header
                .build();
    }

    public <R> List<R> readAll(Function<String[], R> converter) throws IOException, CsvException {
        return csvReader
                .readAll()
                .stream()
                .map(converter)
                .collect(Collectors.toList());
    }


    @Override
    public void close() throws IOException {
        csvReader.close();
    }
}
