package pl.dram.applauseproject.datamanager.csvreader.datamodel;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TesterDeviceRow {
    private long testerId;
    private long deviceId;
}
