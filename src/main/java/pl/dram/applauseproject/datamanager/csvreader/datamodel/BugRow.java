package pl.dram.applauseproject.datamanager.csvreader.datamodel;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BugRow {
    private long bugId;
    private long deviceId;
    private long testerId;
}
