package pl.dram.applauseproject.datamanager.csvreader.mappers.impl;

import pl.dram.applauseproject.datamanager.csvreader.datamodel.TesterRow;
import pl.dram.applauseproject.datamanager.csvreader.mappers.CsvRowMapper;

public class TesterMapper implements CsvRowMapper<TesterRow> {
    @Override
    public TesterRow rowMapper(String[] row) {
        return new TesterRow(
                Long.parseLong(row[0]),
                row[1],
                row[2],
                row[3]
        );
    }
}
