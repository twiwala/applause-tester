package pl.dram.applauseproject.datamanager.csvreader.mappers;

public interface CsvRowMapper<T> {
    T rowMapper(String[] row);
}
