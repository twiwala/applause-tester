package pl.dram.applauseproject.datamanager.csvreader;

import com.opencsv.exceptions.CsvException;
import pl.dram.applauseproject.datamanager.csvreader.mappers.CsvRowMapper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class CsvFileReaderUtils {
    public <T> List<T> readFileAndMap(String fileName, CsvRowMapper<T> mapper) {
        try (CsvFileReader fileReader = new CsvFileReader(fileName)) {
            return fileReader.readAll(mapper::rowMapper);
        } catch (IOException | CsvException | URISyntaxException e) {
            throw new IllegalStateException("Cannot read a file!", e);
        }
    }
}
