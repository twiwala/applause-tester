package pl.dram.applauseproject.datamanager.csvreader.datamodel;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TesterRow {
    private long testerId;
    private String firstName;
    private String lastName;
    private String country;
}
