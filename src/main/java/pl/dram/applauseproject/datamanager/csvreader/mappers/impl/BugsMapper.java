package pl.dram.applauseproject.datamanager.csvreader.mappers.impl;

import pl.dram.applauseproject.datamanager.csvreader.datamodel.BugRow;
import pl.dram.applauseproject.datamanager.csvreader.mappers.CsvRowMapper;

public class BugsMapper implements CsvRowMapper<BugRow> {
    @Override
    public BugRow rowMapper(String[] row) {
        return new BugRow(
                Long.parseLong(row[0]),
                Long.parseLong(row[1]),
                Long.parseLong(row[2])
        );
    }
}
