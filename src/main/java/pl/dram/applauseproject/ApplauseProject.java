package pl.dram.applauseproject;

import pl.dram.applauseproject.datamanager.DataLoader;
import pl.dram.applauseproject.datamanager.TesterDataLoader;
import pl.dram.applauseproject.datamanager.csvreader.CsvFileReaderUtils;
import pl.dram.applauseproject.selector.SelectResult;
import pl.dram.applauseproject.selector.filters.TesterFilter;

import java.util.List;
import java.util.Scanner;

public class ApplauseProject {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        TesterFilter testerFilter = readCriteria();
        List<SelectResult> result = loadData(testerFilter);
        System.out.println(result);
    }

    public static TesterFilter readCriteria() {
        System.out.println("Country filter:");
        String countryFilter = scanner.nextLine();
        System.out.println("Device filter:");
        String deviceFilter = scanner.nextLine();
        return new TesterFilter.TesterFilterBuilder()
                .countryFilter(countryFilter)
                .deviceFilter(deviceFilter)
                .build();
    }

    private static List<SelectResult> loadData(TesterFilter testerFilter) {
        return new TesterDataLoader(new DataLoader(new CsvFileReaderUtils()))
                .getResultByFilter(testerFilter);
    }
}
